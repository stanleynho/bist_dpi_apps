#!/usr/bin/python
import sys
import getopt
import os
import time
import re
import shutil
import getopt
import zipfile
import errno
from subprocess import Popen, PIPE

def main(argv):

   try:
      opts, args = getopt.getopt(argv, 'cbhps', ['clean', 'binary', 'help','pack','source'])

   except getopt.GetoptError:
      usage()
      sys.exit(0)

   for opt, args in opts:
      if opt in ('-h', '--help'):
         usage()

      elif opt in ('-c','--clean'):
         remove_folders("\log")
         remove_files()

      elif opt in ('-b','--binary'):
         make_binary()

      elif opt in ('-p','--pack'):
         pack()

      elif opt in ('-s','--source'):
         make_source()

      else:
         print " wth?"

def usage():
   print "How to use this script\n"
   print "\n"
   print "-b [binary] : make a binary application\n"
   print "-c [clean]: delete all the binary file, logs and result\n"
   print "-h [help]: show the usage\n"
   print "-p [pack]: pack all the neccesary file for delivery into a zip file\n"

def pack():

   # check if the folder is exist then remove it
   if os.path.exists('DPI_GUI'):
      print "DPI_GUI exist so remove it\n"
      shutil.rmtree('DPI_GUI')

   # build the application
   make_binary()

   os.rename("dist","DPI_GUI")

   destPath=os.path.join(os.getcwd(),'DPI_GUI')

   copyDirectoryIntoDir('XML', 'DPI_GUI')
   copyDirectoryIntoDir('scripts', 'DPI_GUI')

   copyFiletoDirectory('pthreadGC2.dll', 'DPI_GUI')
   copyFiletoDirectory('logging.conf', 'DPI_GUI')
   copyFiletoDirectory('dpi_help.txt', 'DPI_GUI')

   shutil.make_archive('DPI_GUI', 'zip', 'DPI_GUI')


def copyDirectoryIntoDir(src, dstPath, symlinks = False, ignore = None):
   """Copy a Folder to other Folder
   """
   parentPath=os.path.join(os.getcwd(),dstPath)
   dst = os.path.join(parentPath,src)

   if not os.path.exists(dst):
      os.makedirs(dst)
      shutil.copystat(src, dst)
   lst = os.listdir(src)
   if ignore:
      excl = ignore(src, lst)
      lst = [x for x in lst if x not in excl]
   for item in lst:
      s = os.path.join(src, item)
      d = os.path.join(dst, item)
      if symlinks and os.path.islink(s):
         if os.path.lexists(d):
            os.remove(d)
         os.symlink(os.readlink(s), d)
         try:
            st = os.lstat(s)
            mode = stat.S_IMODE(st.st_mode)
            os.lchmod(d, mode)
         except:
            pass # lchmod not available
      elif os.path.isdir(s):
         copytree(s, d, symlinks, ignore)
      else:
         shutil.copy2(s, d)

def copyDirectory(src, dst, symlinks = False, ignore = None):
   """Copy file inside src to dst

      Args:

   """
   if not os.path.exists(dst):
      os.makedirs(dst)
      shutil.copystat(src, dst)
   lst = os.listdir(src)
   if ignore:
      excl = ignore(src, lst)
      lst = [x for x in lst if x not in excl]
   for item in lst:
      s = os.path.join(src, item)
      d = os.path.join(dst, item)
      if symlinks and os.path.islink(s):
         if os.path.lexists(d):
            os.remove(d)
         os.symlink(os.readlink(s), d)
         try:
            st = os.lstat(s)
            mode = stat.S_IMODE(st.st_mode)
            os.lchmod(d, mode)
         except:
            pass # lchmod not available
      elif os.path.isdir(s):
         copytree(s, d, symlinks, ignore)
      else:
         shutil.copy2(s, d)

def copyFiletoDirectory(file, dst):
   if not os.path.exists(dst):
      os.makedirs(dst)


   if os.path.exists(file):
      shutil.copy2(file,dst)

   else:
      print("%s not found" % file)

def make_source():
   """Copy all the source file to the source_code foler
      including the bist_dpi and dll files
   """

   if os.path.exists("source_code"):
      shutil.rmtree("source_code")

   for f in os.listdir('.'):
      if re.search('.py',f) or re.search(".dll",f) or re.search(".conf",f):
         copyFiletoDirectory(f,"source_code")

   copyFiletoDirectory("release_note.txt","source_code")
   copyFiletoDirectory("bist_dpi.exe","source_code")

def make_binary():
   print "Make a binary application"
   if not os.path.exists("C:\Python27\Scripts\pyinstaller.exe"):
      print(" There is no pyinstaller for making binary file\n")
      sys.exit(1)

   command_line = "C:\Python27\Scripts\pyinstaller.exe --onefile DPi_GUI.py"
   print command_line
   process = Popen(command_line)#,stdin=PIPE, stdout=PIPE, stderr=PIPE)
   output, error = process.communicate()
   print output
   print "Done"

def remove_folders(folder_name):

   path = os.path.join(os.getcwd(),folder_name)
   print path
   if os.path.exists(folder_name):
      print ("remove the %s folder" % folder_name)
      shutil.rmtree(path)

def remove_files():
   print "Remove all  .pyc, .html and .csv files"
   for f in os.listdir('.'):
      if re.search('.csv', f) or re.search('.pyc', f) or re.search('.html',f) or re.search('.htm',f) or re.search('.spec',f):
         os.remove (f)
   remove_folders("build")
   remove_folders("dist")

if __name__ == "__main__":
   main(sys.argv[1:])
