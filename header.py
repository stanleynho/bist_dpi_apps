import sys
import traceback
import logging
import logging.config




logger = logging.getLogger(__name__)

EXIT_FAILURE                     = 0
FALSE                            = 0
EXIT_SUCCESS                     = 1
TRUE                             = 1
BIST_HANDSHAKE_FAILURE           = 2
BIST_HANDSHAKE_SUCCESS           = 3
COMPORT_OPEN                     = 4
COMPORT_CLOSE                    = 5


def exception_msg(e=None):
      ''' Handle Exception 
         output traceback
         Args:
            e (error); error

         Returns:

      '''
      traceback.print_exc(file=sys.stdout)
      #exc_type, exc_obj, exc_tb = sys.exc_info()
      #fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
      #print "Type: %s \nfilename: %s \nline no: %s \n%s" % (exc_type, fname, exc_tb.tb_lineno, e)

def PRINT(self, text):
      logger.info ( text)
      self.textDialog.config ( state='normal')
      self.textDialog.insert('end', text)
      self.textDialog.yview_pickplace("end")
      self.textDialog.config ( state='disable')
