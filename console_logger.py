##### import from python public module #####
import os
import serial
import time
import sys

class Console_Logger(object):
   """
   redirect log to terminal and log file
   """
   def __init__(self,path, filename="console.log"):
      self.terminal = sys.stdout
      self.log = open(os.path.join(path,filename), "a")

   def write(self, message):
      self.terminal.write(message)
      self.log.write(message)
      self.terminal.flush()
      self.log.flush()