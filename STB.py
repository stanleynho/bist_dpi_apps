import os
import serial
import time
import sys
import string
import datetime
import HTML
import traceback
import logging
import logging.config
from subprocess import Popen, PIPE

import xml.etree.ElementTree as ET

import header as header


##### Loading logger configuration #####
logging.config.fileConfig('logging.conf', disable_existing_loggers=False)
logger = logging.getLogger(__name__)

class STB(object):
   '''
      STB class
   '''

   result_colors = {
        'Passed':      'lime',
        'Failed':      'red',
        'Error' :      'yellow',
    }

   app = 'bist_dpi.exe'
   result_file_name = ''
   GFX_LINE_HEIGHT=30

   current_line = 0
   test_number = 0


   def __init__(self, port_name, dialog):
      """ Intialize the STB object
         Args:
            port_name (str): com port

         Returns:
      """
      self.com_port     = port_name
      self.textDialog = dialog

   def PRINT(self, text):
      logger.info ( text)
      self.textDialog.config ( state='normal')
      self.textDialog.insert('end', text)
      self.textDialog.yview_pickplace("end")
      self.textDialog.config ( state='disable')

   def bist_handshake(self):
      """ doing bist handshake time out in 2 mins
         Args:

         Returns:
            header.EXIT_FAILURE: if fail to connect to the DUT
            header.EXIT_SUCCESS: if connect
      """

      try:
         path = os.path.dirname(os.path.abspath(__file__))
         appPath = os.path.join(path, self.app)
         appPath = appPath.rstrip("\n")

         command = 'connect 255'
         command_line = "%s %s %s %s" % (self.app,"-port", str(self.com_port).upper(), command)
         logger.debug(command_line)

         self.PRINT ("Connecting  to the DUT. ")
         process = Popen(command_line,stdin=PIPE, stdout=PIPE, stderr=PIPE)
         output, error = process.communicate()
         logger.debug(output)
         self.show_on_text_dialog(output)

         if "connect:8400:USB not found" in output:
            self.PRINT ( "Could not open this " + str(self.com_port) + " port")
            return header.EXIT_FAILURE

         elif "Operation timeout" in output:
            self.PRINT ( "Operation timeout ")
            return header.EXIT_FAILURE

         else :
            self.PRINT ( "CONNECTED\n")
            return header.EXIT_SUCCESS

      except IOError,e:
         self.exception_msg(e)
         return header.EXIT_FAILURE

      except :
         self.exception_msg()
         return header.EXIT_FAILURE

   def add_result_to_dict(self, result):
      ''' add result to dict for creating html report
      UNFINISHED
      '''

      lst_data = []
      lst = input.split('\n')

      for item in lst:
         if command in item :
            command_result = item.strip("\r\n")

         else:
            data = data + item.strip("\r\n") +"|"


   def show_on_text_dialog(self, content):
      self.textDialog.config ( state='normal')
      self.textDialog.insert('end', content)
      self.textDialog.yview_pickplace("end")
      self.textDialog.config ( state='disable')


   def write_to_log(self, input, command_line):
      '''Write the result to logs
         Args:
            input             (str): the output from the bist command
            command_line      (str): commnad line sent to bist.exe
         Returns:

      '''

      data="%s," % command_line
      status=""

      # get command not found from bist
      if "not found" in input:
         pass

      # got how to use bist_dpi.exe ###
      elif "Usage:" in input:
         pass

      else:
         lst_command = command_line.split(' ')
         lst_input = input.split('\n')
         command = lst_command[3]

         for item in lst_input :
            if command in item :
               [command, status] = item.strip("\r").split(":",1)

            else:
               data = "%s %s ," % (data , item.strip("\r").strip("\n"))
         result = "%s,%s,%s\n" % ( command, status, data.rstrip(','))

         try:
            log_file = open(self.result_file_name, 'a+')
            log_file.write(result)
            log_file.flush()
            return header.EXIT_SUCCESS

         except IOError,e:
            self.exception_msg(e)
            return header.EXIT_FAILURE

   def send_dpi_cmd_display(self, command):
      '''use for Sending a bist command
         Args:
            command (str): command line

         Returns:

      '''
      try :

         if "help" in command:
            timeout =""

         elif "-timeoutACK" in command:
            timeout = ''
         else:
            timeout = '-timeoutACK 3'

         path = os.path.dirname(os.path.abspath(__file__))
         appPath = os.path.join(path,self.app)
         appPath = appPath.rstrip("\n")

         command_line = "%s %s %s %s %s" % (self.app,"-port", str(self.com_port).upper(), command, timeout)
         logger.debug(command_line)

         process = Popen(command_line,stdin=PIPE, stdout=PIPE, stderr=PIPE)
         output, error = process.communicate()

         self.test_number +=1

         if process.wait() == None :
            self.PRINT (  " %s Failed" % command_line )
            self.print_info( self.current_line, str(self.test_number)+ ":" + command+ ":Failed")

         else:
            self.PRINT ( '%s : %s' % (command , output))
            self.print_info( self.current_line, str(self.test_number)+ ":" + command + output)
            if self.write_to_log(output, command_line) == header.EXIT_FAILURE:
               exit()

         self.current_line += 1

         # reset the current line when it hit a bottom of the screen.
         if self.current_line ==15 :
            self.current_line = 0

         return output
      except :
         self.exception_msg()
         exit()



   def send_dpi_cmd(self, command):
      '''use for Sending a bist command
         Args:
            command (str): command line

         Returns:

      '''
      try :

         if "help" in command:
            timeout =""

         elif "-timeoutACK" in command:
            timeout = ''
         else:
            timeout = '-timeoutACK 3'

         path = os.path.dirname(os.path.abspath(__file__))
         appPath = os.path.join(path,self.app)
         appPath = appPath.rstrip("\n")

         command_line = "%s %s %s %s %s" % (self.app,"-port", str(self.com_port).upper(), command, timeout)
         logger.debug(command_line)

         process = Popen(command_line,stdin=PIPE, stdout=PIPE, stderr=PIPE)
         output, error = process.communicate()

         if process.wait() == None :
            self.PRINT ("running ... "  )

         else :
            self.PRINT ( output )

            if "help" not in command:
               self.write_to_log(output, command_line)
            return header.EXIT_SUCCESS

         return output

      except :
         self.exception_msg()
         exit()

   def draw_text_on_TV(self, command):
      '''use for Sending a bist command
         Args:
            command (str): command line

         Returns:

      '''

      try :

         if "help" in command:
            timeout =""

         else:
            timeout = '-timeoutACK 3'

         command_line = "%s %s %s %s %s" % (self.app,"-port", str(self.com_port).upper(), command, timeout)
         logger.debug(command_line)

         process = Popen(command_line,stdin=PIPE, stdout=PIPE, stderr=PIPE)
         output, error = process.communicate()

         if process.wait() == None :
            #self.PRINT ( "running ... "  )
            print "Running...."

         else :
            print output
            return header.EXIT_SUCCESS

      except :
         self.exception_msg()
         exit()

   def run_xml_script(self, script_name):
      '''Run a script of commands
         Args:
            script_name (str): script name

         Returns:
            header.EXIT_SUCCESS
            header.EXIT_FAILURE
      '''
      timeout = '-timeoutACK 3'
      current_line = 0
      test_number = 0
      self.clear_screen()
      try:
         tree = ET.parse(script_name)
         root = tree.getroot()

         for child in root.iter('Test'):
            for element in child:

               if element.tag == "message":
                  ##### Got a message print to stdout #####
                  print element.text

               elif element.tag == "comment":
                     ##### got a comment do nothing #####
                     pass

               elif element.tag == "sleep":
                     print "sleep" + element.text
                     time.sleep(int(element.text))

               elif element.tag == "command_no_display":

                  #run the command without print_info on the TV
                  line = element.text
                  command_line = "%s %s %s %s %s" % (self.app, "-port", str(self.com_port).upper(), line.rstrip('\n'), timeout)
                  logger.debug( command_line )

                  process = Popen(command_line,stdin=PIPE, stdout=PIPE, stderr=PIPE)
                  output, error = process.communicate()

               elif element.tag == "display":
                     line = element.text
                     self.print_info( current_line,line.strip() )
                     self.current_line += 1

               elif element.tag == "command":
                  line = element.text
                  command_line = "%s %s %s %s %s" % (self.app, "-port", str(self.com_port).upper(), line.rstrip('\n'), timeout)
                  logger.debug( command_line )
                  self.test_number +=1

                  process = Popen(command_line,stdin=PIPE, stdout=PIPE, stderr=PIPE)
                  output, error = process.communicate()
                  #self.show_on_text_dialog(output)

                  if process.wait() == None :
                     print" %s Failed" % command_line
                     self.print_info( current_line, str(test_number)+ ":" + line.strip() + ":Failed")

                  else:
                     #self.PRINT (output)
                     self.print_info( current_line, str(test_number)+ ":" + line.strip() + output)

                     if self.write_to_log(output, command_line) == header.EXIT_FAILURE:
                        exit()

                  self.current_line += 1
                  # reset the current line when it hit a bottom of the screen.
                  if self.current_line ==15 :
                     self.current_line = 0
      except :
         self.exception_msg()
         return header.EXIT_FAILURE

   def run_txt_script(self, script_name):
      '''Run a script of commands
         Args:
            script_name (str): script name

         Returns:
            header.EXIT_SUCCESS
            header.EXIT_FAILURE
      '''
      timeout = '-timeoutACK 3'
      current_line = 0
      test_number = 0
      self.clear_screen()
      try:
         with open( script_name ) as file:
            for line in file:
               if line.strip():
                  if "sleep" in line:
                     self.PRINT ( line)
                     lst_sleep = line.split()
                     time.sleep(int(lst_sleep[1]))

                  elif "#" in line:
                     ##### got a comment do nothing #####
                     pass

                  elif "@" in line:
                     ##### Got a message print to stdout #####
                     self.PRINT ( line)

                  elif "%" in line:
                     #run the command without print_info on the TV
                     line = line.replace("%","")
                     command_line = "%s %s %s %s %s" % (self.app, "-port", str(self.com_port).upper(), line.rstrip('\n'), timeout)
                     logger.debug ( command_line )

                     process = Popen(command_line,stdin=PIPE, stdout=PIPE, stderr=PIPE)
                     output, error = process.communicate()
                     self.show_on_text_dialog(output)

                  elif "&" in line:
                     line = line.replace("&","")
                     self.print_info( current_line,line.strip() )
                     current_line += 1

                  else:
                     command_line = "%s %s %s %s %s" % (self.app, "-port", str(self.com_port).upper(), line.rstrip('\n'), timeout)
                     logger.debug (command_line )
                     test_number +=1

                     process = Popen(command_line,stdin=PIPE, stdout=PIPE, stderr=PIPE)
                     output, error = process.communicate()
                     #self.show_on_text_dialog(output)

                     if process.wait() == None :
                        self.PRINT (  " %s Failed" % command_line )
                        self.print_info( current_line, str(test_number)+ ":" + line.strip() + ":Failed")
                     else:
                        self.PRINT ( output)
                        self.print_info( current_line, str(test_number)+ ":" + line.strip() + output)
                        if self.write_to_log(output, command_line) == header.EXIT_FAILURE:
                           exit()

                     current_line += 1
                     # reset the current line when it hit a bottom of the screen.
                     if current_line ==15 :
                        current_line = 0

      except IOError,e:
         print "\nCould not open script file %s" % script_name
         return header.EXIT_FAILURE




   def get_line_pos(self, line):
      '''
      '''
      first_line = 40
      line_height = self.GFX_LINE_HEIGHT

      return first_line + (line_height * (line -1))

   def clear_line(self, line_number):
      '''
      '''
      command = "gfxDrawRectangle 0 %s 720 %s blue" % (str(self.get_line_pos(line_number)), self.GFX_LINE_HEIGHT )
      self.draw_text_on_TV( command )

   def clear_screen(self):
      '''
      '''
      cmd = "gfxScreen cleanUpdate blue"
      self.draw_text_on_TV(cmd)

   def print_info(self, line, text):
      '''
      '''
      self.clear_line(line)
      # Draw text
      cmd = 'gfxDrawText white noChange 0 %s 720 center "%s" ' % (str(self.get_line_pos(line)), text)
      logger.debug(cmd)
      self.draw_text_on_TV(cmd)

      #update screen
      cmd = "gfxScreen update"
      self.draw_text_on_TV(cmd)


   def print_highlight(self,line_number, text_color, bg_clolor):
      '''
      '''
      clear_line(line)
      # Draw text
      cmd = "gfxDrawText" + text_color + " " + bg_color +" 0 " + self.get_line_pos + " 720 center" + text
      self.draw_text_on_TV(cmd)

      #update screen
      cmd = "gfxScreen update"
      self.draw_text_on_TV(cmd)


   def exception_msg(self,e=None):
      ''' Handle Exception
         output traceback
         Args:
            e (error); error

         Returns:

      '''
      traceback.print_exc(file=sys.stdout)
      #exc_type, exc_obj, exc_tb = sys.exc_info()
      #fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
      #print "Type: %s \nfilename: %s \nline no: %s \n%s" % (exc_type, fname, exc_tb.tb_lineno, e)
