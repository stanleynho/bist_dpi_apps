#checking stb
versionGetRID
versionGetBIST

#checking cables
keyCapFPCableTest
#keyResetPressedCount
ledCapLEDs
keyFPCableTest

#testing LEDs
ledSetState all off
sleep 2
ledSetState all on
sleep 2
ledSetState all dim
sleep 2
ledSetState all off

ledSetState allNoStandby on
sleep 2
ledSetState allNoStandby dim
sleep 2
ledSetState allNoStandby off
sleep 2

ledSetState power on
sleep 2
ledSetState power dim
sleep 2
ledSetState power off
sleep 2

ledSetState record on
sleep 2
ledSetState record dim
sleep 2
ledSetState record off
sleep 2

ledSetState backlight on
sleep 2
ledSetState backlight dim
sleep 2
ledSetState backlight off
sleep 2

ledSetLuminosity all 30
ledSetLuminosity all 60
ledSetLuminosity all 100
ledSetLuminosity all 0

ledSetLuminosity allNoStandby 30
ledSetLuminosity allNoStandby 60
ledSetLuminosity allNoStandby 100
ledSetLuminosity allNoStandby 0

ledSetLuminosity power 30
ledSetLuminosity power 60
ledSetLuminosity power 100
ledSetLuminosity power 0

ledSetLuminosity record 30
ledSetLuminosity record 60
ledSetLuminosity record 100
ledSetLuminosity record 0

ledSetLuminosity backlight 30
ledSetLuminosity backlight 60
ledSetLuminosity backlight 100
ledSetLuminosity backlight 0

ledSetLuminosity hd480i 30
ledSetLuminosity hd480i 60
ledSetLuminosity hd480i 100
ledSetLuminosity hd480i 0

ledSetLuminosity hd480p 30
ledSetLuminosity hd480p 60
ledSetLuminosity hd480p 100
ledSetLuminosity hd480p 0

ledSetLuminosity hd720p 30
ledSetLuminosity hd720p 60
ledSetLuminosity hd720p 100
ledSetLuminosity hd720p 0

ledSetLuminosity 1080i 30
ledSetLuminosity 1080i 60
ledSetLuminosity 1080i 100
ledSetLuminosity 1080i 0

ledSetLuminosity 1080p 30
ledSetLuminosity 1080p 60
ledSetLuminosity 1080p 100
ledSetLuminosity 1080p 0

#testing patern
@doing the led pattern test
ledSetPattern error
sleep 5 
ledSetPattern dst 
sleep 5
ledSetPattern temperature 
sleep 5
ledSetPattern allAlternate 
sleep 5
ledSetPattern breathe 
sleep 5
ledSetPattern locked 
sleep 5 

ledSetState all on






