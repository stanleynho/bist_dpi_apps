# import from python public module #####
import serial
import time
import sys
import tkMessageBox
import os
import HTML
import shutil
import logging
import logging.config
import xml.etree.ElementTree as ET
import tkFileDialog
import collections

from ctypes  import *
from Tkinter import *
import Tkinter as tk
from threading import Thread, Lock
from console_logger import Console_Logger

#from AutoScrollBar import *
import header as header
from STB import STB




##### Loading logger configuration #####
logging.config.fileConfig('logging.conf', disable_existing_loggers=header.FALSE)
logger = logging.getLogger(__name__)

def create_folder(folder_name):
   '''Create a folder for logs
      Args:
         Folder (str): folder name
      
      Returns:
         1
         0
   '''
   try:
      if not os.path.exists(folder_name):
         os.makedirs(folder_name)
   
   except IOError,e:
      print "\nCould not create folder"
      exception_msg(e)
      return header.EXIT_FAILURE
   
   return header.EXIT_SUCCESS


class GUI(tk.Tk):
   
   loadXMLSuccess = header.FALSE
   loadTXTSuccess = header.FALSE
   
   connectDUTSuccess = header.FALSE
   display_line = 0
   test_number = 0
   comPort = ''
   testResultName = ''
   
   def availablePorts(self):
      for i in range(256):
         try:
            s = serial.Serial(i)
            s.close()
            yield 'COM' + str(i + 1)
         except serial.SerialException, e:
            pass
   
   def PRINT(self, text):
      logger.info ( text)
      self.outputDialog.config ( state='normal')
      self.outputDialog.insert('end', text)
      self.outputDialog.yview_pickplace("end")
      self.outputDialog.config ( state='disable')
      
   def load_txt_script(self):
   
      try:
         scriptType = [('DOC file', '*.txt')]
          
         scriptFile =  tkFileDialog.askopenfile (parent = self.tkRoot,  title = 'Load  script file', filetypes = scriptType, mode = 'r')    
         logger.debug(scriptFile)
         
         # remove the text dialog first 
         if  self.loadXMLSuccess ==  header.TRUE:
            self.canvas.pack_forget()   
            self.canvasFrame.pack_forget()
            self.vscrollbar.pack_forget()
            self.hscrollbar.pack_forget()
         
         # remove the old textDialog
         if self.loadTXTSuccess ==  header.TRUE:
            self.textDialog.pack_forget()         
            
         loadedFile = scriptFile.read()
         scriptFile.close()
         self.textDialog = Text(self.testFrame)
         self.textDialog.pack(fill = BOTH, expand =1)
         
         self.textDialog.insert("end", loadedFile)
         
         self.loadXMLSuccess =  header.FALSE
         self.loadTXTSuccess = header.TRUE
         logger.debug("load tx script")
         
         
      except:    
         header.exception_msg()   
      
   def get_xml_file_name(self):
      fileType = [('XML file', '*.xml')]
         
      try: 
      
         xmlFileName =  tkFileDialog.askopenfilename (parent = self.tkRoot,  title = 'Load xml file', filetypes = fileType)    
         print xmlFileName
         
         if 'xml' in xmlFileName:
            self.load_the_xml_checkBox(xmlFileName)   
            
      except:    
         header.exception_msg()
         
   def load_the_xml_checkBox(self,xmlFileName):
   
      try :
         tree = ET.parse(xmlFileName)
         self.xmlRoot = tree.getroot()
         
         #create a check box list    
         rowCount=0
         
         # remove the text dialog first 
         
         if self.loadTXTSuccess == header.TRUE:
            self.textDialog.pack_forget()         
         
         if self.loadXMLSuccess == header.TRUE :
            self.canvas.pack_forget()   
            self.canvasFrame.pack_forget()
            self.vscrollbar.pack_forget()
            self.hscrollbar.pack_forget()
         
         self.testFrame.config(width=300,height=300)
         
         #create canvas
         self.canvas = Canvas(self.testFrame) 
         
         #create the scrollbar
         self.vscrollbar = Scrollbar(self.testFrame,orient=VERTICAL)
         self.vscrollbar.pack(side=RIGHT, fill=Y)   
         self.vscrollbar.config(command=self.canvas.yview)
         
         self.hscrollbar = Scrollbar(self.testFrame, orient=HORIZONTAL)
         self.hscrollbar.pack(side=BOTTOM, fill=X)   
         self.hscrollbar.config(command=self.canvas.xview)
         
         #bind the scrollbar to canvas
         self.canvas.config(width=300,height=300)
         self.canvas.config(xscrollcommand=self.hscrollbar.set, yscrollcommand=self.vscrollbar.set)
                   
         self.canvas.config(scrollregion=(0,0,5000,5000))
         #self.canvas.config(scrollregion=self.canvas.bbox("ALL"))
         #self.canvas.bind_all("<MouseWheel>", self._on_mousewheel)
         self.canvas.pack(side=LEFT,expand=True,fill=BOTH)
         
         #create the canvasFrame
         self.canvasFrame = Frame(self.canvas , bg= "yellow", width=300,height=300)  
         self.canvasFrame.pack(fill = BOTH , expand = 1)
            
         self.canvas.create_window(0, 0, anchor=NW, window=self.canvasFrame)
         self.canvasFrame.update_idletasks()

         #create the unsorteddict to store the name of the checkbox
         self.unsortedCheckBoxDict.clear()
         self.checkBoxDict.clear()
         
         for child in self.xmlRoot.iter('Test'):
            self.unsortedCheckBoxDict[child.get("name")] = IntVar()
         
         #sorted the unsorted dict
         self.checkBoxDict = collections.OrderedDict(sorted(self.unsortedCheckBoxDict.items()))
         
         #create the check list and put it on the canvas        
         for key in self.checkBoxDict:
            checkBoxList = Checkbutton(self.canvasFrame, text=key , variable = self.checkBoxDict[key])
            checkBoxList.grid( row = rowCount, column =0, sticky = W)
            
            #
            rowCount = rowCount + 1
         
         #make sure all the checkbox are unchecked
         for item in self.checkBoxDict:
            self.checkBoxDict[item].set(0)
        
         self.loadXMLSuccess =header.TRUE
         self.loadTXTSuccess = header.FALSE
         
         logger.debug("load xml script")
         
      except : 
         header.exception_msg()
   
   def _on_mousewheel(self, event):
      '''
         local function for mouse wheel
      '''
   
      self.canvas.yview_scroll(-1*(event.delta/120), "units")  
   
   def run_test(self):
      '''
         a function call when run button pressed'
      '''
      
      try:
         if self.connectDUTSuccess == header.EXIT_SUCCESS:
         
            #create a new thread for the running the xml file
            if self.loadXMLSuccess == header.TRUE:
               logger.debug("Run test from xml file")
               self.dut_thread = Thread( target = self.do_run_xml_test) 
               self.dut_thread.setDaemon(header.TRUE)
               self.dut_thread.start()  

            #create a new thredd for running the script file
            elif self.loadTXTSuccess == header.TRUE:
               logger.debug("Run test from script txt file")
               self.dut_thread = Thread( target = self.do_run_script_test) 
               self.dut_thread.setDaemon(header.TRUE)
               self.dut_thread.start() 
               
            else:
               self.PRINT( "No script to run")
         else:
            self.PRINT( "NOT CONNECT TO DUT")
      
      except:       
         header.exception_msg()   

   def do_run_script_test(self):
      try:
         # read the input from the text dialog
         textInput = self.textDialog.get("1.0",END)
         logger.debug (textInput)
         
         #create a temp file to run the script.
         with open("script.bin", 'w+') as f:
            
            f.write(textInput)
         self.DUT.run_txt_script("script.bin")
         
         #remove file
         os.remove("script.bin")
         
      except:
         header.exception_msg() 
         
   def do_run_xml_test(self):   
      
      if self.loadXMLSuccess == header.TRUE:
         
         for key in  self.checkBoxDict:
            if  self.checkBoxDict.get(key).get() == 1:
               
               for child in self.xmlRoot.iter('Test'):
                  
                  if child.get('name') == key:

                     for element in child:
                        
                        if element.tag == 'sleep':
                           print "sleep  " + element.text
                           time.sleep(int(element.text))
                        
                        elif element.tag == 'message':
                           print element.text
                            
                        elif element.tag == 'display':
                           self.DUT.print_info(self.display_line,element.text)

                        else:
                           self.DUT.send_dpi_cmd_display( element.text)
            
   def check_all(self):
      '''
         check all the checkboxes
      '''
      try:      
         if self.loadXMLSuccess == header.TRUE:
            for key in self.checkBoxDict:
               self.checkBoxDict.get(key).set(1) 
               
      except: 
         header.exception_msg()
         
   def uncheck_all(self):
      '''
         uncheck all the checkboxes
      '''
      
      try:
         if self.loadXMLSuccess == header.TRUE:
            for key in self.checkBoxDict:
               self.checkBoxDict.get(key).set(0) 
      
      except: 
         header.exception_msg() 
   
   def do_connect_DUT(self):
      try:
         self.DUT = STB(self.comPort, self.outputDialog )
         
         # checking for the test result name         
         if len( self.testResultName)  :
            
            #using a new name
            self.DUT.result_file_name = self.testResultName
        
         else:
            self.DUT.result_file_name = 'test_result.csv'
            
         self.connectDUTSuccess = self.DUT.bist_handshake()
      
      except: 
         header.exception_msg() 
         
   def connect_DUT(self):
      """connect to DUT
         run the doTest Thread
         disable connect DUT button
         disable com list
      Args: 
         
      Returns:
      """
     
      try:
         if "COM DUT" not in self.comPort:   

            #reset the display line on tv and the test number 
            self.display_line = 0
            self.test_number = 0
            
            self.comPort = portSelected.get()
            
            #start the thread
            self.dut_thread = Thread( target = self.do_connect_DUT )
            self.dut_thread.setDaemon(header.TRUE)
            self.dut_thread.start()  
      except: 
         header.exception_msg() 
   
   def send_dpi_command(self,cmd):
      try:
         if cmd :
            if self.connectDUTSuccess == header.EXIT_SUCCESS:
               self.DUT.send_dpi_cmd(cmd)
         
      except:
          header.exception_msg() 
   
   def export_to_html(self):
      '''
      export the result to the html file
      '''
      fType = [('HTML', 'html')]
      try:
         htmlFileName =  tkFileDialog.asksaveasfilename (parent = self.tkRoot,  title = 'export log to html', filetypes = fType) 
         if os.path.isfile('test_result.csv') == False:
            print " %s file does not exist to create the html report" % result_file_name
            return header.EXIT_FAILURE
        
         test_results = []         
         if "html"  not in htmlFileName or "htm"  not in htmlFileName:
            htmlFileName = htmlFileName + '.html'
         
         logfile = open(htmlFileName ,'w+')
         table = HTML.Table(header_row=['Test', 'Result','Command','Data'])
         
         with open('test_result.csv','r') as file: 
            for line in file :
               list_line = line.rstrip().rstrip("\n").rstrip(",").split(',')
               
               #create the coloured cell:
               if list_line[1] == "Passed":
                  color = "lime"
               
               else:
                  color = "yellow"
               
               colored_result = HTML.TableCell(list_line[1], bgcolor=color)
               list_line[1]=colored_result
               table.rows.append(list_line)    
         
         htmlcode = str(table)
         logfile.write(htmlcode)
         
         print "Done generating the %s.html" % htmlFileName
      
      except : 
         header.exception_msg()
   
   def send_call_back(self,event):
      '''
         function for bind with the return or enter key
      '''
      self.send_dpi_command(self.dpiCmdEntry.get())
      
   def save_as(self):
      fType = [('CVS','csv')]
      csvFileName = tkFileDialog.asksaveasfilename (parent = self.tkRoot,  title = 'Save test result to csv', filetypes = fType) 
      
      logger.debug( csvFileName)
      
      if "csv"  not in htmlFileName :
            csvFileName = htmlFileName + '.csv'
         
      if os.path.isfile(csvFileName) == header.TRUE:
         
         #there is a file already, ask for overwrtite
         retval = tkMessageBox.askyesno(title = 'Warning', message = 'File already exist. Overwrite?' )
         if retval == header.TRUE:
            self.testResultName = csvFileName
            logger.debug(self.testResultName)
      
      else:
         print csvFileName
         self.testResultName = csvFileName
   
   def help(self):
   
      helpString =("For Script File\n" 
                        "--------------------------------------------------------------------------\n" 
                        "line start with #: this is the comment. wil lbe ignored \n" 
                        "line start with @: this is a message and will be display on console output \n" 
                        "line start with %: this command will be run but will not be displayed on TV\n"
                        "line start with & : this is not a command, it is a message and will be displayed on the TV\n"
                        "Example:\n"
                        "#AV test\n"
                        "@======================\n"
                        "versionGetBIST\n"
                        "%gfxScreen cleanUpdate transparent\n"
                        "sleep 10\n"
                        "&DONE\n"
                        "--------------------------------------------------------------------------\n"                         
                        "For XML File\n"
                        "<Test name=> tag: start the test with name "
                        "<message> tag : this is a message and will be display on console output\n"
                        "<command> tag : this is a command will run by bist_dpi\n"
                        "<display> tag: this is not a command, it is a message and will be displayed on the TV\n "
                        "<sleep> tag: will pause the application for x amount of second\n"
                        )
                        
      tkMessageBox.showinfo("Help", helpString)
   
   def about(self):
      aboutString =("Bist Dpi GUI\n"
                          "Version: 1.1"
                          )
      tkMessageBox.showinfo("Help", aboutString)
   
   def __init__(self):
            
      # Create a TK root 
      self.tkRoot = Tk()
      self.xmlFileName =""
      self.xmlRoot = ""
      self.checkBoxDict = dict()
      self.unsortedCheckBoxDict = dict()
      
      # Get available com port 
      AvailableCOMPorts = list(self.availablePorts())

      self.tkRoot.geometry('500x800')
      self.tkRoot.title("DPI GUI")
      self.tkRoot.pack_propagate(header.FALSE)
      
      # create a menu bar 
      menuBar = Menu(self.tkRoot)
      
      #create and pulldown  FILE menus
      fileMenu = Menu(menuBar, tearoff=0)
      fileMenu.add_command ( label = "Load xml", command = self.get_xml_file_name)
      fileMenu.add_command ( label = "Load Script", command = self.load_txt_script)
      fileMenu.add_command( label = "Exit", command = self.tkRoot.quit)
      menuBar.add_cascade( label = "File", menu = fileMenu)
      
      #create and pulldown  EDIT menus
      fileMenu = Menu(menuBar, tearoff=0)
      fileMenu.add_command ( label = "Check All", command = self.check_all)
      fileMenu.add_command( label = "Clear All", command = self.uncheck_all)
      menuBar.add_cascade( label = "Edit", menu = fileMenu)
            
      #create and pulldown  TEST menus
      fileMenu = Menu(menuBar, tearoff=0)
      fileMenu.add_command ( label = "Save As", command = self.save_as)
      #fileMenu.add_command( label = "Delete Log", command = self.uncheck_all)
      fileMenu.add_command( label = "Export To HTML", command = self.export_to_html)
      menuBar.add_cascade( label = "Log", menu = fileMenu)
      
      #create and pulldown  Help menus
      fileMenu = Menu(menuBar, tearoff=0)
      fileMenu.add_command ( label = "Help", command = self.help)
      fileMenu.add_command( label = "About", command = self.about)
      menuBar.add_cascade( label = "Help", menu = fileMenu)
      
      # display the menu
      self.tkRoot.config(menu=menuBar)
      
      # create a control Frame for button and com port selection  
      controlFrame  = Frame(self.tkRoot, bg ="red" , width =500, height = 30)
      controlFrame.pack(fill = X)
      
      # create a comport selection
      global portSelected 
      portSelected = StringVar()
      portSelected.set("COM DUT")
      
      comList = apply(OptionMenu, (controlFrame, portSelected) + 
                                                   tuple(["COM DUT"]) + tuple(AvailableCOMPorts))
      
      comList.config(width = 10 )
      comList.grid(row = 0, column = 0, sticky = W+N+S+E)
      
      #create CONNECT button      
      connectButton = Button(controlFrame , text="Connect", 
                                       command = lambda: self.connect_DUT())      
      connectButton.grid(row = 0, column = 1, sticky = W+N+S+E)
      connectButton.config(width = 10 )
      
      #create send button      
      runButton = Button(controlFrame , text="Run", 
                                       command = lambda: self.run_test())     
      runButton.grid(row = 0, column = 2, sticky = W+N+S+E)
      runButton.config(width = 10 )
      
      # create a frame to for canvas
      self.testFrame = Frame(self.tkRoot , height = 500)  
      
      # make the canvas expandable
      self.testFrame.grid_rowconfigure(0, weight=1)
      self.testFrame.grid_columnconfigure(0, weight=1)
      self.testFrame.pack(fill = BOTH, expand = 1 )
      
      # create a frame for entry
      dpiFrame = Frame(self.tkRoot , width =  700 , height = 30, bg= "purple")  
      dpiFrame.pack(fill = X)
      
      # create a label 
      dpiLabel = Label(dpiFrame, text  = "DPI Command", width = 13)
      dpiLabel.pack(side = LEFT)
      
      # create a CMD button
      self.dpiCmdEntry = Entry(dpiFrame)
      self.dpiCmdEntry.pack(side = LEFT, fill = X, expand = 1)
      self.dpiCmdEntry.bind('<Return>',self.send_call_back)
      
      #create send button      
      sendButton = Button(dpiFrame , text="Send", 
                                       command = lambda: self.send_dpi_command(self.dpiCmdEntry.get()))      
      
      sendButton.pack(side = LEFT)
      sendButton.bind('<Return>',self.send_call_back)
      
      # create a frame for output
      outputFrame = Frame(self.tkRoot , height = 50, bg= "blue")  
      outputFrame.pack(fill = X)
      
      scrollbar = Scrollbar(outputFrame)
      scrollbar.pack(side=RIGHT, fill=Y)
      
      self.outputDialog = Text(outputFrame, wrap=WORD, yscrollcommand=scrollbar.set)
      self.outputDialog.pack(fill = BOTH, expand =1)
      
      self.outputDialog.yview_pickplace("end")

      scrollbar.config(command=self.outputDialog.yview)
      
      #start mainloop for GUI
      self.tkRoot.mainloop()
   
if __name__ == "__main__":
   create_folder("logs")
   sys.stdout = Console_Logger("logs","console_output.txt")
   win = GUI()
