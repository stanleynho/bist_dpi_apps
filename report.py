import HTML
def create_html_report():
      ''' carete HTM report
      UNFINISHED
      '''
      test_results                     = []
      log_file_name = 'bist_dpi.csv'
      logfile = open("report.html",'w+')
      t = HTML.Table(header_row=['Test', 'Result','Data'])
      with open(log_file_name,'r') as file: 
         for line in file :
            list_line = line.rstrip().rstrip("\n").rstrip(",").split(',')
            #create the coloured cell:
            if list_line[1] == "Passed":
               color = "lime"
            else:
               color = "yellow"
            colored_result = HTML.TableCell(list_line[1], bgcolor=color)
            list_line[1]=colored_result
            t.rows.append(list_line)    
      htmlcode = str(t)
      logfile.write(htmlcode)
if __name__ == "__main__":
   print "Creating report.html from bist_pdi.csv"
   create_html_report()